# binaries

![](header_image.jpg)

## Overview

Binaries for any tools or applications.

## Table of binaries and platforms

Binary  | Windows | Linux | Mac
--- | --- | --- | ---
datediff | Yes | No | No
